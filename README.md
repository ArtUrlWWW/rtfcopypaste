Introduction

   Module to copy/paste Netbeans editor content with formatting to RTF format.

Usage
 
   1. Select code,

   2. Click right mouse button to expand popup menu,

   3. Select "Copy as RTF format" option to copy, selected content to system clipboard,

   4. Paste clipboard content to some RTF editor like OpenOffice? or MSWord (special paste).

Additional options

   1. Go to Tools - Options

   2. Select Copy/Paste tab to customize plugin options.

Compability

   I tested plugin on Netbeans 6.5 and 7.0M1 with OpenOffice? and MSOffice.

Netbeans plugin page, for:

6.5:

http://plugins.netbeans.org/PluginPortal/faces/PluginDetailPage.jsp?pluginid=16089

6.5.1:

http://plugins.netbeans.org/PluginPortal/faces/PluginDetailPage.jsp?pluginid=16957

6.7.1:

http://plugins.netbeans.org/PluginPortal/faces/PluginDetailPage.jsp?pluginid=22470